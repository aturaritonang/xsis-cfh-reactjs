import React from "react";

class Room extends React.Component {
    render(){
        const { name, subject } = this.props;
        return (
            <p>Room : {name}, Subject: {subject}</p>
        )
    }
}

export default Room;